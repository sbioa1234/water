// +build dragonfly freebsd netbsd openbsd

package water

import (
	"fmt"
	"os"
	"path/filepath"
)

func newTAP(config Config) (ifce *Interface, err error) {
	file, err := openDevice("tap")
	if err != nil {
		return nil, err
	}

	ifce = &Interface{isTAP: true, ReadWriteCloser: file, name: filepath.Base(file.Name())}
	return
}

func newTUN(config Config) (ifce *Interface, err error) {
	file, err := openDevice("tun")
	if err != nil {
		return nil, err
	}

	ifce = &Interface{isTAP: false, ReadWriteCloser: file, name: filepath.Base(file.Name())}
	return
}

func openDevice(devname string) (file *os.File, err error) {
	for i := 0; i < 256; i++ {
		dev := fmt.Sprintf("%s%d", devname, i)
		filename := fmt.Sprintf("/dev/%s", dev)
		file, err = os.OpenFile(filename, os.O_RDWR, 0)
		if err == nil {
			break
		}
	}
	return
}
